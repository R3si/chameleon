#!/bin/bash

set -e

# Creates a session with session cookie

user=$GF_SECURITY_ADMIN_USER
password=$GF_SECURITY_ADMIN_PASSWORD

curl -c ~/tmp_cookie \
  -H 'Host: localhost:3000' \
  -H 'content-type: application/json' \
  -H 'Origin: https://localhost:3000' \
  --data "{\"user\": \"${user}\",
         \"password\": \"${password}\"}" \
  http://localhost:3000/login

# Get login attempts data from the last Scan period

get_logins() {
  current_time=$(date +%s000)
  scan_span=300000 #$(($1 * 1000))
  scan_time=$(($current_time - $scan_span)) # Scan time as parameter
  sql_request="SELECT \$__time(date),\
(data->>'server') as service,\
(data->>'status') as login_status,\
(data->>'ip') as attacker_ip,\
(data->>'username') as username,\
(data->>'password') as password \
FROM servers_table WHERE \$__timeFilter(date) AND (data->>'ip') NOT LIKE '0.0.0.0' AND (data->>'status') NOT LIKE 'NULL'"
  curl -s -k \
  -b ~/tmp_cookie \
    -H 'Host: localhost:3000' \
    -H 'content-type: application/json' \
    --data-binary "{\"from\": \"${scan_time}\",
         \"to\": \"${current_time}\",
         \"queries\": [{\"refId\":\"A\",
              \"intervalMs\":60000,
              \"maxDataPoints\":816,
              \"datasourceId\":1,
              \"rawSql\":\"${sql_request}\",
              \"format\":\"table\"}]}" \
  http://localhost:3000/api/tsdb/query
}

# Get port access data from the last Scan period

get_ports() {
  current_time=$(date +%s000)
  scan_span=300000 #$(($1 * 1000)) # Sec to milsec
  scan_time=$(($current_time - $scan_span)) # Scan time as parameter
  sql_request="SELECT \$__time(date),\
(data->>'dst_port') as host_port,\
(data->>'src_ip') as attacker_ip,\
(data->>'mac') as attacker_mac \
FROM sniffer_table WHERE \$__timeFilter(date) AND (data->>'dst_port') NOT LIKE '1900' AND (data->>'src_ip') NOT LIKE '0.0.0.0'" #Doesn´t select own interaction
  curl -s -k \
    -H 'Host: localhost:3000' \
    -H 'content-type: application/json' \
    -b ~/tmp_cookie \
    --data-binary "{\"from\": \"${scan_time}\",
         \"to\": \"${current_time}\",
         \"queries\": [{\"refId\":\"A\",
              \"intervalMs\":60000,
              \"maxDataPoints\":816,
              \"datasourceId\":1,
              \"rawSql\":\"${sql_request}\",
              \"format\":\"table\"}]}" \
  http://localhost:3000/api/tsdb/query
}

# Get Results from DB and make them processable

response_logins=$(get_logins $scan_span)
response_ports=$(get_ports $scan_span)
row_count_logins=$(echo "$response_logins" | jq '.results.A.meta.rowCount' )
row_count_ports=$(echo "$response_ports" | jq '.results.A.meta.rowCount' )

# Check if there have been logins attempts in the last scan_span seconds
if [ "$row_count_logins" -gt 0 ]; then
  echo "Got $row_count_logins results in logins"
  var=$(echo "$response_logins" | jq '.results.A.tables[0].rows')
  webhook_data="{\"${KUNDE}\":\"login_attempt\", \"data\": ${var}}"
  curl -d "${webhook_data}" $WEBHOOK_URL
else
  echo "Empty result from logins ($row_count_logins)"
fi

# Check if there have been port scans in the last scan_span seconds
if [ "$row_count_ports" -gt 0 ]; then
  echo "Got $row_count_ports results in ports"
  var=$(echo "$response_ports" | jq '.results.A.tables[0].rows')
  webhook_data="{\"${KUNDE}\":\"port_activity\", \"data\": ${var}}"
  curl -d "${webhook_data}" $WEBHOOK_URL
else
  echo "Empty result from ports ($row_count_ports)"
fi