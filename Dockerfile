FROM docker.io/grafana/grafana:7.5.7
USER root
RUN apk add --no-cache jq curl
RUN (crontab -l 2>/dev/null; echo "*/5 * * * * /home/grafana/webhook_alert.sh") | crontab -
RUN ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
      echo 'Europe/Berlin' > /etc/timezone
USER grafana
COPY ./postgres.yml /etc/grafana/provisioning/datasources/postgres.yml
COPY ./dashboards.yml /etc/grafana/provisioning/dashboards/dashboards.yml
COPY ./chameleon.json /var/lib/grafana/dashboards/chameleon.json
COPY ./webhook_alert.sh /home/grafana/webhook_alert.sh
